const myData = require('./data')

const result = myData.reduce((startValue,person)=> {
    if(person.email.endsWith('.org') || person.email.endsWith('.au') || person.email.endsWith('.com')) {
        startValue += 1 
    }
    return startValue
},0)

console.log(result)