const myData = require('./data')

const result = myData.map((person) => {
    let full_name = person.first_name + " " + person.last_name ;
    person['fullName'] = full_name
    return person
})

console.log(result)