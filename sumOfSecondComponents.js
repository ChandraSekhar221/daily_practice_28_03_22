const myData = require('./data')

const result = myData.reduce((startingValue,person)=> {
    let secondValue = parseFloat(person.ip_address.split(".")[1])
    return startingValue + secondValue
},0)

console.log(result)