const myData = require("./data");

const firstNameArray = myData.map((person) => person.first_name).sort()

const result = firstNameArray.reduce((startValue,firstName) => {
    myData.forEach((person) => {
        if(firstName == person.first_name) {
            startValue.push(person)
        }
    })
    return startValue
},[])

console.log(result)
