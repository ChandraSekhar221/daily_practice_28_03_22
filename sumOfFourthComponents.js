const myData = require('./data')

const result = myData.reduce((startingValue,person)=> {
    let fourthValue = parseFloat(person.ip_address.split(".")[3])
    return startingValue + fourthValue
},0)

console.log(result)